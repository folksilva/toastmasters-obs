# Ferramentas para funções em reuniões do Toastmasters usando o OBS

Esse conjunto de ferramentas permite que você utilize o OBS Studio para criar uma experiência inovadora nas suas reuniões on-line do Toastmasters.

Esse documento irá te guiar no passo a passo de como configurar do zero e usar as ferramentas.


## Sumário

1. Instalando o OBS Studio
2. Instalando o Plugin de Navegador
3. Instalando o Plugin de Camera Virtual
4. Criando um projeto gratuito no Google Firebase
5. Configurando o Google Firebase na sua máquina
6. Configurando o seu projeto do Google Firebase
7. Implantando o projeto no Google Firebase
8. Criando uma cena no OBS Studio
9. Configurando o Zoom para usar sua Camera Virtual
10. Controlando as ferramentas durante a reunião 