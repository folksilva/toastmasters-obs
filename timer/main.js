document.addEventListener('DOMContentLoaded', function () {

    try {
        let app = firebase.app()
        app.database().ref('/background').on('value', snapshot => {
            const { color } = snapshot.val()
            const boxEl = document.getElementById('box')
            if (boxEl) boxEl.className = color
        })
        app.database().ref('/effects').on('value', snapshot => {
            const { applause, suspense, thumbs } = snapshot.val()
            if (applause) {
                document.getElementById('applause').className = 'active'
            } else {
                document.getElementById('applause').className = ''
            }
            if (suspense) {
                document.getElementById('suspense').className = 'active'
            } else {
                document.getElementById('suspense').className = ''
            }
            if (thumbs) {
                document.getElementById('thumbs').className = 'active'
            } else {
                document.getElementById('thumbs').className = ''
            }
        })
        app.database().ref('/grammarian').on('value', snapshot => {
            const { word, definition } = snapshot.val()
            const daywordEl = document.getElementById('dayword')
            console.log(daywordEl, word, definition)
            if (daywordEl) {
                const wordEl = document.querySelector('#dayword h2')
                const definitionEl = document.querySelector('#dayword p')
                if (word) {
                    daywordEl.className = 'active'
                    wordEl.innerHTML = word
                    if (definition) {
                        definitionEl.innerHTML = definition
                    } else {
                        definitionEl.innerHTML = ''
                    }
                } else {
                    daywordEl.className = ''
                    wordEl.innerHTML = ''
                    definitionEl.innerHTML = ''
                }
            }
        })
    } catch (e) {
        console.error(e)
    }
})
